<?php

namespace TodoList\Tests\UserInterface\Integration\TodoList;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TodoList\Tests\UserInterface\ApiTestCase;

/**
 * Class CreateListTest
 * @package TodoList\Tests\UserInterface\Integration
 */
class CreateListTest extends ApiTestCase
{
    /**
     * @var string
     */
    private string $url;

    public function setUp(): void
    {
        parent::setUp();

        $this->url = $this->getUrlByRouteName('create_list');
    }

    /**
     * @group user_interface
     * @group create_list
     */
    public function testCreateList(): void
    {
        $response = $this->apiRequest($this->url, Request::METHOD_POST, ['title' => 'test title']);

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    /**
     * @group user_interface
     * @group create_list
     */
    public function testListNameAlreadyExistError(): void
    {
        $parameters = ['title' => 'test title'];

        $response = $this->apiRequest($this->url, Request::METHOD_POST, $parameters);

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        $response = $this->apiRequest($this->url, Request::METHOD_POST, $parameters);

        $this->assertEquals(Response::HTTP_CONFLICT, $response->getStatusCode());
        $this->assertEquals('{"error":"List with the same title already exists."}', $response->getContent());
    }

    /**
     * @group user_interface
     * @group create_list
     */
    public function testEmptyListTitleError(): void
    {
        $parameters = ['title' => ''];

        $response = $this->apiRequest($this->url, Request::METHOD_POST, $parameters);

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertEquals('{"error":"Title must not be empty."}', $response->getContent());
    }
}
