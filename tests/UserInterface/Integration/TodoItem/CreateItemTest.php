<?php

namespace TodoList\Tests\UserInterface\Integration\TodoItem;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TodoList\Domain\ValueObjects\Status;
use TodoList\Tests\UserInterface\ApiTestCase;

/**
 * Class CreateListTest
 * @package TodoList\Tests\UserInterface\Integration
 */
class CreateItemTest extends ApiTestCase
{
    /**
     * @var string
     */
    private string $url;

    /**
     * @var array
     */
    private array $defaultParameters;

    public function setUp(): void
    {
        parent::setUp();

        $this->url = $this->getUrlByRouteName('create_item', ['listId' => $this->createList()]);

        $this->defaultParameters = [
            'title' => 'test title',
            'status' => Status::getInProgressStatus(),
            'note' => 'test note',
        ];
    }

    /**
     * @group user_interface
     * @group create_note
     */
    public function testNote(): void
    {
        $response = $this->apiRequest($this->url, Request::METHOD_POST, $this->defaultParameters);
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    /**
     * @group user_interface
     * @group create_note
     */
    public function testNoteWithTheSameTitleAlreadyExistError(): void
    {
        $response = $this->apiRequest($this->url, Request::METHOD_POST, $this->defaultParameters);
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        $response = $this->apiRequest($this->url, Request::METHOD_POST, $this->defaultParameters);
        $this->assertEquals('{"error":"Task with the same title already exists."}', $response->getContent());
        $this->assertEquals(Response::HTTP_CONFLICT, $response->getStatusCode());
    }

    /**
     * @group user_interface
     * @group create_note
     */
    public function testListNotFoundError(): void
    {
        $listId = PHP_INT_MAX;
        $url = $this->getUrlByRouteName('create_item', ['listId' => $listId]);
        $response = $this->apiRequest($url, Request::METHOD_POST, $this->defaultParameters);
        $this->assertEquals('{"error":"List with id=' . $listId . ' not found."}', $response->getContent());
        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    /**
     * @group user_interface
     * @group create_note
     */
    public function testEmptyNoteTitleError(): void
    {
        $parameters = [
            'title' => '',
            'status' => Status::getInProgressStatus(),
            'note' => 'test note',
        ];

        $response = $this->apiRequest($this->url, Request::METHOD_POST, $parameters);

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertEquals('{"error":"Title must not be empty."}', $response->getContent());
    }

    /**
     * @group user_interface
     * @group create_note
     */
    public function testNoteStatusError(): void
    {
        $parameters = [
            'title' => 'test title',
            'status' => '',
            'note' => 'test note',
        ];

        $response = $this->apiRequest($this->url, Request::METHOD_POST, $parameters);

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertEquals('{"error":"Status must not be empty."}', $response->getContent());

        $parameters = [
            'title' => 'test title',
            'status' => 'test_note_status',
            'note' => 'test note',
        ];

        $response = $this->apiRequest($this->url, Request::METHOD_POST, $parameters);

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertEquals('{"error":"Status must be one of these values: todo, in_progress, done."}', $response->getContent());
    }

    /**
     * @return int
     */
    private function createList(): int
    {
        $url = $this->getUrlByRouteName('create_list');
        $response = $this->apiRequest($url, Request::METHOD_POST, ['title' => 'test title']);
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);

        return $content['id'];
    }
}
