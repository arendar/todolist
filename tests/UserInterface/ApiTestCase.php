<?php

namespace TodoList\Tests\UserInterface;

use Doctrine\ORM\EntityManagerInterface;
use FOS\OAuthServerBundle\Model\ClientManagerInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use OAuth2\OAuth2;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class ApiTestCase
 * @package TodoList\Tests\UserInterface
 */
class ApiTestCase extends WebTestCase
{
    private const GRANT_TYPE = 'password';
    private const USER_NAME = 'test_user';
    private const USER_PASSWORD = 'test_user';
    private const USER_EMAIL = 'test_user@example.com';

    /**
     * @var KernelBrowser
     */
    private KernelBrowser $client;

    /**
     * @var object|RouterInterface|null
     */
    private ?RouterInterface $router;

    /**
     * @var EntityManagerInterface
     */
    private ?EntityManagerInterface $em;

    /**
     * @var ClientManagerInterface
     */
    private ClientManagerInterface $clientManager;

    /**
     * @var string
     */
    private string $clientId;

    /**
     * @var string
     */
    private string $clientSecret;

    /**
     * @var UserManagerInterface
     */
    private ?UserManagerInterface $userManipulator;

    /**
     * @var string|null
     */
    private ?string $token;

    public function setUp(): void
    {
        $this->client = self::createClient();
        $this->client->disableReboot();
        $this->clientManager = self::$container->get(ClientManagerInterface::class);
        $this->userManipulator = self::$container->get(UserManagerInterface::class);

        $this->router = self::$container->get(RouterInterface::class);

        $this->em = self::$container->get('doctrine.orm.default_entity_manager');

        $this->em->beginTransaction();

        $this->setToken();
    }

    /**
     * @param string $url
     * @param string $method
     * @param array $parameters
     * @param array $files
     * @param array $headers
     * @return Response
     */
    protected function apiRequest(
        string $url,
        string $method = Request::METHOD_GET,
        array $parameters = [],
        array $files = [],
        array $headers = []
    ): Response {
        $headers = array_merge(['HTTP_AUTHORIZATION' => "Bearer $this->token"], $headers);

        $this->client->request($method, $url, $parameters, $files, $headers);

        return $this->client->getResponse();
    }

    /**
     * @param string $routeName
     * @param array $parameters
     * @return string
     */
    protected function getUrlByRouteName(string $routeName, array $parameters = []): string
    {
        return $this->router->generate($routeName, $parameters);
    }

    private function setToken(): void
    {
        $this->createApiClient();
        $this->createApiUser();

        $authUrl = $this->router->generate('fos_oauth_server_token');

        $parameters = [
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'response_type' => OAuth2::RESPONSE_TYPE_ACCESS_TOKEN,
            'grant_type' => OAuth2::GRANT_TYPE_USER_CREDENTIALS,
            'username' => self::USER_NAME,
            'password' => self::USER_PASSWORD,
        ];

        $this->client->request(Request::METHOD_POST, $authUrl, $parameters);
        $response = $this->client->getResponse();
        $content = \json_decode($response->getContent(), true);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertArrayHasKey('access_token', $content);

        $this->token = $content['access_token'];
    }

    private function createApiClient(): void
    {
        $client = $this->clientManager->createClient();

        $client->setAllowedGrantTypes([self::GRANT_TYPE]);

        $this->clientManager->updateClient($client);

        $this->clientId = $client->getPublicId();
        $this->clientSecret = $client->getSecret();
    }

    private function createApiUser(): void
    {
        $user = $this->userManipulator->createUser();
        $user->setUsername(self::USER_NAME);
        $user->setEmail(self::USER_EMAIL);
        $user->setPlainPassword(self::USER_PASSWORD);
        $user->setEnabled(true);
        $user->setSuperAdmin(true);
        $this->userManipulator->updateUser($user);
    }

    public function tearDown(): void
    {
        $this->em->rollback();
    }
}
