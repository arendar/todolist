<?php

namespace TodoList\Tests\Domain\Unit\ValueObjects;

use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use TodoList\Domain\Exceptions\InvalidStatusException;
use TodoList\Domain\ValueObjects\Status;

/**
 * Class StatusTest
 * @package TodoList\Tests\Domain\Unit\ValueObjects
 */
class StatusTest extends TestCase
{
    /**
     * @test
     * @group domain
     * @group value_objects
     * @param string $value
     * @param string $errorMessage
     * @dataProvider invalidValuesDataProvider
     */
    public function expectForInvalidValueException(string $value, string $errorMessage): void
    {
        $this->expectException(InvalidStatusException::class);
        $this->expectErrorMessage($errorMessage);
        new Status($value);
    }

    /**
     * @test
     * @group domain
     * @group value_objects
     * @dataProvider validStatusesDataProvider
     * @param string $value
     */
    public function checkForValidValue(string $value): void
    {
        $valueObject = new Status($value);
        $value = trim($value);

        $this->assertEquals($value, (string)$valueObject);
        $this->assertEquals($value, $valueObject->getValue());
    }

    /**
     * @return array|string[]
     */
    public function validStatusesDataProvider(): array
    {
        return [
            [Status::getTodoStatus()],
            [Status::getInProgressStatus()],
            [Status::getDoneStatus()],
            [' todo    ']
        ];
    }

    /**
     * @return array|string[]
     */
    public function invalidValuesDataProvider(): array
    {
        return [
            ['', 'Status must not be empty.'],
            ['test_value', 'Status must be one of these values: todo, in_progress, done.'],
            ['     ', 'Status must not be empty.'],
        ];
    }
}
