<?php

namespace TodoList\Tests\Domain\Unit\ValueObjects;

use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use TodoList\Domain\Exceptions\InvalidTitleException;
use TodoList\Domain\ValueObjects\Title;

/**
 * Class TitleText
 * @package TodoList\Tests\Domain\Unit\ValueObjects
 */
class TitleTest extends TestCase
{
    private const TOO_LONG_STRING = 'yhsPk2lNzQ92TeVOTCrRoMNI3sAjuCoplun8NPlNnoGAAl7xpT97jg1jnSGNH5FKQBo8uRARJmw92qqMMOHtBv6pZzYXMBIfsMPWS71ihGTBx8yZBYnjMyy6xYauqZ1Fu46z7CSME75Bb8ssbtZ2HNKYUBdeAVU0dDvJBpKhIxExPtMA0w3ZOQbKpZfeBRu9z4CEBaiBJyUJszk5AdckkECWvgFxqPJxp66KzOMpBzrHEdQrV8MuIWA5KwRtU1hi453MtS2N4OxDATAvwvDHKrJlRlNMPFFtHOzyfvUtDnBE';

    /**
     * @test
     * @group domain
     * @group value_objects
     */
    public function checkForValidTitle(): void
    {
        $testTitle = 'test title';
        $titleObject = new Title($testTitle);

        $this->assertEquals($testTitle, (string)$titleObject);
        $this->assertEquals($testTitle, $titleObject->getValue());
    }

    /**
     * @test
     * @group domain
     * @group value_objects
     */
    public function expectForInvalidTitleExceptionWithEmptyTitle(): void
    {
        $this->expectErrorMessage('Title must not be empty.');
        $this->expectException(InvalidTitleException::class);
        new Title('');
    }

    /**
     * @test
     * @group domain
     * @group value_objects
     */
    public function expectForInvalidTitleExceptionWithOnlySpacerTitle(): void
    {
        $this->expectErrorMessage('Title must not be empty.');
        $this->expectException(InvalidTitleException::class);
        new Title('     ');
    }

    /**
     * @test
     * @group domain
     * @group value_objects
     */
    public function expectForInvalidTitleExceptionWithTooLongValue(): void
    {
        $this->expectErrorMessage('Title is too long (maximum is 255 characters).');
        $this->expectException(InvalidTitleException::class);
        new Title(self::TOO_LONG_STRING);
    }
}
