<?php

namespace TodoList\Domain\Interfaces;

/**
 * Interface DomainEntity
 * @package TodoList\Domain\Entities
 */
interface DomainEntity
{
    /**
     * @return array
     */
    public function toArray(): array;
}
