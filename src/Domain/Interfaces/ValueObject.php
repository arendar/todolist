<?php

namespace TodoList\Domain\Interfaces;

/**
 * Interface AbstractValueObject
 * @package TodoList\Domain\ValueObjects
 */
interface ValueObject
{
    /**
     * @return mixed
     */
    public function getValue();
}
