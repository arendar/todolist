<?php

namespace TodoList\Domain\Entities;

use DateTime;
use TodoList\Domain\ValueObjects\Status;
use TodoList\Domain\ValueObjects\Title;

/**
 * Class TodoItem
 * @package TodoList\Domain\Entities
 */
class TodoItem extends BaseEntity
{
    /**
     * @var int
     */
    protected int $id;

    /**
     * @var int
     */
    protected int $listId;

    /**
     * @var string
     */
    protected string $title;

    /**
     * @var string
     */
    protected string $status;

    /**
     * @var string|null
     */
    protected ?string $note;

    /**
     * @var DateTime
     */
    protected DateTime $createdAt;

    /**
     * @var DateTime
     */
    protected DateTime $updatedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getListId(): int
    {
        return $this->listId;
    }

    /**
     * @param int $listId
     * @return $this
     */
    public function setListId(int $listId): self
    {
        $this->listId = $listId;
        return $this;
    }

    /**
     * @return Title
     */
    public function getTitle(): Title
    {
        return new Title($this->title);
    }

    /**
     * @param Title $title
     * @return $this
     */
    public function setTitle(Title $title): self
    {
        $this->title = (string)$title;
        return $this;
    }

    /**
     * @return Status
     */
    public function getStatus(): Status
    {
        return new Status($this->status);
    }

    /**
     * @param Status $status
     * @return $this
     */
    public function setStatus(Status $status): self
    {
        $this->status = (string)$status;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * @param string|null $note
     * @return $this
     */
    public function setNote(?string $note): self
    {
        $this->note = $note;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     * @return $this
     */
    public function setUpdatedAt(DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
