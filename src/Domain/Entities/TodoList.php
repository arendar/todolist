<?php

namespace TodoList\Domain\Entities;

use DateTime;
use TodoList\Domain\ValueObjects\Title;

/**
 * Class TodoList
 * @package TodoList\Domain\Entities
 */
class TodoList extends BaseEntity
{
    /**
     * @var int
     */
    protected int $id;

    /**
     * @var int
     */
    protected int $userId;

    /**
     * @var string
     */
    protected string $title;

    /**
     * @var DateTime
     */
    protected DateTime $createdAt;

    /**
     * @var DateTime
     */
    protected DateTime $updatedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId(int $userId): self
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return Title
     */
    public function getTitle(): Title
    {
        return new Title($this->title);
    }

    /**
     * @param Title $title
     * @return $this
     */
    public function setTitle(Title $title): self
    {
        $this->title = (string)$title;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     * @return $this
     */
    public function setUpdatedAt(DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
