<?php

namespace TodoList\Domain\Entities;

use TodoList\Domain\Interfaces\DomainEntity;

/**
 * Class BaseEntity
 * @package TodoList\Domain\Entities
 */
abstract class BaseEntity implements DomainEntity
{
    /**
     * @return array
     */
    public function toArray(): array
    {
        return get_object_vars($this);
    }
}
