<?php

namespace TodoList\Domain\Exceptions;

use InvalidArgumentException;

/**
 * Class InvalidStatusException
 * @package TodoList\Domain\Exceptions
 */
class InvalidStatusException extends InvalidArgumentException
{

}
