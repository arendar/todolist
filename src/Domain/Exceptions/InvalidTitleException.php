<?php

namespace TodoList\Domain\Exceptions;

use InvalidArgumentException;

/**
 * Class InvalidTitleException
 * @package TodoList\Domain\Exceptions
 */
class InvalidTitleException extends InvalidArgumentException
{

}
