<?php

namespace TodoList\Domain\ValueObjects;

use TodoList\Domain\Exceptions\InvalidStatusException;
use TodoList\Domain\Interfaces\ValueObject;

/**
 * Class Status
 * @package TodoList\Domain\ValueObjects
 */
class Status implements ValueObject
{
    private const IN_PROGRESS = 'in_progress';
    private const TODO = 'todo';
    private const DONE = 'done';

    /**
     * @var string
     */
    private string $value;

    /**
     * Status constructor.
     * @param string $value
     * @throws InvalidStatusException
     */
    public function __construct(string $value)
    {
        $value = $this->clearValue($value);

        $this->assertNotEmptyStatus($value);
        $this->assertNotValidStatus($value);

        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @throws InvalidStatusException
     */
    private function assertNotValidStatus(string $value): void
    {
        if (!in_array($value, static::getStatuses(), true)) {
            $message = 'Status must be one of these values: ' . implode(', ', static::getStatuses()) . '.';
            throw new InvalidStatusException($message);
        }
    }

    /**
     * @param string $value
     * @throws InvalidStatusException
     */
    public function assertNotEmptyStatus(string $value): void
    {
        if (empty($value)) {
            throw new InvalidStatusException('Status must not be empty.');
        }
    }

    /**
     * @param string $value
     * @return string
     */
    private function clearValue(string $value): string
    {
        return trim($value);
    }

    /**
     * @return array|string[]
     */
    public static function getStatuses(): array
    {
        return [
            self::TODO,
            self::IN_PROGRESS,
            self::DONE,
        ];
    }

    /**
     * @return string
     */
    public static function getTodoStatus(): string
    {
        return self::TODO;
    }

    /**
     * @return string
     */
    public static function getInProgressStatus(): string
    {
        return self::IN_PROGRESS;
    }

    /**
     * @return string
     */
    public static function getDoneStatus(): string
    {
        return self::DONE;
    }
}
