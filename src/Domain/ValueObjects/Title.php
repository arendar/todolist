<?php

namespace TodoList\Domain\ValueObjects;

use TodoList\Domain\Exceptions\InvalidTitleException;
use TodoList\Domain\Interfaces\ValueObject;

/**
 * Class Title
 * @package TodoList\Domain\ValueObjects
 */
class Title implements ValueObject
{
    private const MAX_TITLE_LENGTH = 255;

    /**
     * @var string
     */
    private string $value;

    /**
     * Title constructor.
     * @param string $value
     * @throws InvalidTitleException
     */
    public function __construct(string $value)
    {
        $value = $this->clearValue($value);

        $this->assertNotValidTitle($value);

        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return string
     */
    private function clearValue(string $value): string
    {
        return trim($value);
    }

    /**
     * @param string $value
     * @throws InvalidTitleException
     */
    private function assertNotValidTitle(string $value): void
    {
        if (empty($value)) {
            throw new InvalidTitleException('Title must not be empty.');
        }

        if (strlen($value) > self::MAX_TITLE_LENGTH) {
            $message = sprintf('Title is too long (maximum is %d characters).', self::MAX_TITLE_LENGTH);
            throw new InvalidTitleException($message);
        }
    }
}
