<?php

namespace TodoList\UserInterface\Controller\Web;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApiDocumentationController
 * @package TodoList\Presentation
 */
class ApiDocumentationController extends AbstractController
{
    /**
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('apidoc.html.twig');
    }
}
