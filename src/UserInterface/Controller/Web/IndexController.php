<?php

namespace TodoList\UserInterface\Controller\Web;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class IndexController
 * @package TodoList\Presentation
 */
class IndexController extends AbstractController
{
    /**
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('index.html.twig');
    }
}
