<?php

namespace TodoList\UserInterface\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ExceptionController
 * @package TodoList\Presentation\Controller\Api
 */
class ExceptionController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function showException(Request $request): JsonResponse
    {
        $exception = $request->get('exception');
        return new JsonResponse(['error' => $exception->getMessage()], $exception->getStatusCode());
    }
}
