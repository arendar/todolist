<?php

namespace TodoList\UserInterface\Controller\Api;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use TodoList\Application\Common\Interfaces\Request as TodoRequest;
use TodoList\Application\Interfaces\Mediator;

/**
 * Class Controller
 * @package TodoList\UserInterface\Controller
 */
abstract class Controller extends AbstractFOSRestController
{
    /**
     * @var Mediator
     */
    protected Mediator $mediator;

    /**
     * Controller constructor.
     * @param Mediator $mediator
     */
    public function __construct(Mediator $mediator)
    {
        $this->mediator = $mediator;
    }

    /**
     * @param Request $request
     * @param TodoRequest $todoRequest
     * @return JsonResponse
     */
    protected function getResponse(Request $request, TodoRequest $todoRequest): JsonResponse
    {
        $userId = $this->container
            ->get('security.token_storage')
            ->getToken()
            ->getUser()
            ->getId();

        $todoRequest->setUserId($userId);
        $todoRequest->setParameters($request->request->all());

        $response = $this->mediator->send($todoRequest);

        return new JsonResponse($response->getMessage(), $response->getStatus());
    }

}
