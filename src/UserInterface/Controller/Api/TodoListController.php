<?php

namespace TodoList\UserInterface\Controller\Api;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use TodoList\Application\TodoList\Commands\CreateTodoList\CreateListCommand;
use TodoList\Application\TodoList\Commands\DeleteList\DeleteListCommand;
use TodoList\Application\TodoList\Commands\UpdateTitle\UpdateTitleCommand;
use TodoList\Application\TodoList\Queries\GetItems\GetItemsQuery;
use TodoList\Application\TodoList\Queries\GetList\GetListQuery;
use TodoList\Application\TodoList\Queries\GetLists\GetListsQuery;

/**
 * Class TodoListController
 * @package TodoList\UserInterface\Controller\Api
 */
class TodoListController extends Controller
{
    /**
     * @param Request $request
     * @param CreateListCommand $createListCommand
     * @return JsonResponse
     */
    public function postStore(Request $request, CreateListCommand $createListCommand): JsonResponse
    {
        return $this->getResponse($request, $createListCommand);
    }

    /**
     * @param Request $request
     * @param DeleteListCommand $deleteListCommand
     * @param int $listId
     * @return JsonResponse
     */
    public function deleteList(Request $request, DeleteListCommand $deleteListCommand, int $listId): JsonResponse
    {
        $request->request->add(['list_id' => $listId]);
        return $this->getResponse($request, $deleteListCommand);
    }

    /**
     * @param Request $request
     * @param int $listId
     * @param UpdateTitleCommand $titleCommand
     * @return JsonResponse
     */
    public function patchUpdateTitle(Request $request, UpdateTitleCommand $titleCommand, int $listId): JsonResponse
    {
        $request->request->add(['list_id' => $listId]);
        return $this->getResponse($request, $titleCommand);
    }

    /**
     * @param Request $request
     * @param GetListQuery $getListQuery
     * @param int $listId
     * @return JsonResponse
     */
    public function getList(Request $request, GetListQuery $getListQuery, int $listId): JsonResponse
    {
        $request->request->add(['list_id' => $listId]);
        return $this->getResponse($request, $getListQuery);
    }

    /**
     * @param Request $request
     * @param GetListsQuery $listQuery
     * @return JsonResponse
     */
    public function getLists(Request $request, GetListsQuery $listQuery): JsonResponse
    {
        return $this->getResponse($request, $listQuery);
    }

    /**
     * @param Request $request
     * @param GetItemsQuery $itemsQuery
     * @param int $listId
     * @return JsonResponse
     */
    public function getNotes(Request $request, GetItemsQuery $itemsQuery, int $listId): JsonResponse
    {
        $request->request->add(['list_id' => $listId]);
        return $this->getResponse($request, $itemsQuery);
    }
}
