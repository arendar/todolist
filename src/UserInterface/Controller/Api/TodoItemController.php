<?php

namespace TodoList\UserInterface\Controller\Api;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use TodoList\Application\TodoItem\Commands\CreateTodoItem\CreateTodoItemCommand;
use TodoList\Application\TodoItem\Commands\DeleteItem\DeleteItemCommand;
use TodoList\Application\TodoItem\Commands\UpdateTodoItem\UpdateTodoItemCommand;
use TodoList\Application\TodoItem\Queries\GetItem\GetItemQuery;

/**
 * Class TodoListController
 * @package TodoList\UserInterface\Controller\Api
 */
class TodoItemController extends Controller
{
    /**
     * @param Request $request
     * @param CreateTodoItemCommand $itemCommand
     * @param int $listId
     * @return JsonResponse
     */
    public function postStore(Request $request, CreateTodoItemCommand $itemCommand, int $listId): JsonResponse
    {
        $request->request->add(['list_id' => $listId]);
        return $this->getResponse($request, $itemCommand);
    }

    /**
     * @param Request $request
     * @param UpdateTodoItemCommand $itemCommand
     * @param int $noteId
     * @return JsonResponse
     */
    public function patchUpdateNote(Request $request, UpdateTodoItemCommand $itemCommand, int $noteId): JsonResponse
    {
        $request->request->add(['note_id' => $noteId]);
        return $this->getResponse($request, $itemCommand);
    }

    /**
     * @param Request $request
     * @param DeleteItemCommand $itemCommand
     * @param int $noteId
     * @return JsonResponse
     */
    public function deleteNote(Request $request, DeleteItemCommand $itemCommand, int $noteId): JsonResponse
    {
        $request->request->add(['note_id' => $noteId]);
        return $this->getResponse($request, $itemCommand);
    }

    /**
     * @param Request $request
     * @param GetItemQuery $getItemQuery
     * @param int $noteId
     * @return JsonResponse
     */
    public function getNote(Request $request, GetItemQuery $getItemQuery, int $noteId): JsonResponse
    {
        $request->request->add(['note_id' => $noteId]);
        return $this->getResponse($request, $getItemQuery);
    }
}
