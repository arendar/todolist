<?php

namespace TodoList\Infrastructure\Factories;

use DateTime;
use TodoList\Application\Interfaces\TodoItem\TodoItemFactoryInterface;
use TodoList\Application\Interfaces\TodoItem\TodoItemRepositoryInterface;
use TodoList\Domain\Entities\TodoItem;
use TodoList\Domain\ValueObjects\Status;
use TodoList\Domain\ValueObjects\Title;
use TodoList\Infrastructure\DoctrineEntity\Domain\TodoItem as DoctrineTodoItem;

/**
 * Class TodoItemFactory
 * @package TodoList\Infrastructure\Factories
 */
class TodoItemFactory implements TodoItemFactoryInterface
{
    /**
     * @var int
     */
    private int $id = 0;

    /**
     * @var int
     */
    private int $listId;

    /**
     * @var string
     */
    private string $title;

    /**
     * @var string
     */
    private string $status;

    /**
     * @var string|null
     */
    private ?string $note;

    /**
     * @var DateTime
     */
    private DateTime $createdAt;

    /**
     * @var DateTime
     */
    private DateTime $updatedAt;

    /**
     * @var TodoItemRepositoryInterface
     */
    private TodoItemRepositoryInterface $repository;

    /**
     * TodoItemFactory constructor.
     * @param TodoItemRepositoryInterface $repository
     */
    public function __construct(TodoItemRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return TodoItem
     */
    public function make(): TodoItem
    {
        return $this->getEntityObject()
            ->setId($this->id)
            ->setListId($this->listId)
            ->setTitle(new Title($this->title))
            ->setStatus(new Status($this->status))
            ->setNote($this->note)
            ->setCreatedAt($this->createdAt)
            ->setUpdatedAt($this->updatedAt);
    }

    /**
     * @return DoctrineTodoItem
     */
    private function getEntityObject(): DoctrineTodoItem
    {
        $doctrineEntity = new DoctrineTodoItem();

        if ($this->id > 0) {
            $entity = $this->repository->find($this->id);

            if ($entity) {
                $this->initDefaults($entity);

                $doctrineEntity
                    ->setId($entity->getId())
                    ->setListId($entity->getListId())
                    ->setStatus($entity->getStatus())
                    ->setTitle($entity->getTitle())
                    ->setNote($entity->getNote())
                    ->setUpdatedAt($entity->getUpdatedAt())
                    ->setCreatedAt($entity->getCreatedAt());

            }
        }

        return $doctrineEntity;
    }

    /**
     * @param DoctrineTodoItem $entity
     */
    private function initDefaults(DoctrineTodoItem $entity): void
    {
        $this->setId($this->id ?? $entity->getId());
        $this->setListId($this->listId ?? $entity->getListId());
        $this->setTitle(isset($this->title) ? new Title($this->title) : $entity->getTitle());
        $this->setStatus(isset($this->status) ? new Status($this->status) : $entity->getStatus());
        $this->setNote($this->note ?? $entity->getNote());
        $this->setUpdatedAt($this->updatedAt ?? $entity->getUpdatedAt());
        $this->setCreatedAt($this->createdAt ?? $entity->getCreatedAt());
    }

    /**
     * @param int $id
     * @return $this|TodoItemFactoryInterface
     */
    public function setId(int $id): TodoItemFactoryInterface
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param int $listId
     * @return $this|TodoItemFactoryInterface
     */
    public function setListId(int $listId): TodoItemFactoryInterface
    {
        $this->listId = $listId;

        return $this;
    }

    /**
     * @param Title $title
     * @return $this|TodoItemFactoryInterface
     */
    public function setTitle(Title $title): TodoItemFactoryInterface
    {
        $this->title = (string)$title;

        return $this;
    }

    /**
     * @param Status $status
     * @return $this|TodoItemFactoryInterface
     */
    public function setStatus(Status $status): TodoItemFactoryInterface
    {
        $this->status = (string)$status;
        return $this;
    }

    /**
     * @param string|null $note
     * @return $this|TodoItemFactoryInterface
     */
    public function setNote(?string $note): TodoItemFactoryInterface
    {
        $this->note = $note;
        return $this;
    }

    /**
     * @param DateTime $createdAt
     * @return $this|TodoItemFactoryInterface
     */
    public function setCreatedAt(DateTime $createdAt): TodoItemFactoryInterface
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @param DateTime $updatedAt
     * @return $this|TodoItemFactoryInterface
     */
    public function setUpdatedAt(DateTime $updatedAt): TodoItemFactoryInterface
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
