<?php

namespace TodoList\Infrastructure\Factories;

use DateTime;
use TodoList\Application\Interfaces\TodoList\TodoListFactoryInterface;
use TodoList\Application\Interfaces\TodoList\TodoListRepositoryInterface;
use TodoList\Domain\Entities\TodoList;
use TodoList\Domain\ValueObjects\Title;
use TodoList\Infrastructure\DoctrineEntity\Domain\TodoList as DoctrineTodoList;

/**
 * Class TodoListFactory
 * @package TodoList\Infrastructure\Factories
 */
class TodoListFactory implements TodoListFactoryInterface
{
    /**
     * @var TodoListRepositoryInterface
     */
    private TodoListRepositoryInterface $repository;

    /**
     * @var int
     */
    private int $id = 0;

    /**
     * @var int
     */
    private int $userId;

    /**
     * @var string
     */
    private string $title;

    /**
     * @var DateTime
     */
    private DateTime $createdAt;

    /**
     * @var DateTime
     */
    private DateTime $updatedAt;

    /**
     * TodoListFactory constructor.
     * @param TodoListRepositoryInterface $repository
     */
    public function __construct(TodoListRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return TodoList
     */
    public function make(): TodoList
    {
        return $this->getEntityObject()
            ->setId($this->id)
            ->setUserId($this->userId)
            ->setTitle(new Title($this->title))
            ->setCreatedAt($this->createdAt)
            ->setUpdatedAt($this->updatedAt);
    }

    /**
     * @return DoctrineTodoList
     */
    private function getEntityObject(): DoctrineTodoList
    {
        $doctrineEntity = new DoctrineTodoList();

        if ($this->id > 0) {
            $entity = $this->repository->find($this->id);

            if ($entity) {
                $this->initDefaults($entity);

                $doctrineEntity
                    ->setId($entity->getId())
                    ->setUserId($entity->getUserId())
                    ->setTitle($entity->getTitle())
                    ->setCreatedAt($entity->getCreatedAt())
                    ->setUpdatedAt($entity->getUpdatedAt());
            }
        }

        return $doctrineEntity;
    }

    /**
     * @param DoctrineTodoList $entity
     */
    public function initDefaults(DoctrineTodoList $entity): void
    {
        $this->setUserId($this->userId ?? $entity->getUserId());
        $this->setId($this->id ?? $entity->getId());
        $this->setCreatedAt($this->createdAt ?? $entity->getCreatedAt());
        $this->setUpdatedAt($this->updatedAt ?? $entity->getUpdatedAt());
        $this->setTitle(isset($this->title) ? new Title($this->title) : $entity->getTitle());
    }

    /**
     * @param int $id
     * @return $this|TodoListFactoryInterface
     */
    public function setId(int $id): TodoListFactoryInterface
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param int $userId
     * @return $this|TodoListFactoryInterface
     */
    public function setUserId(int $userId): TodoListFactoryInterface
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @param Title $title
     * @return $this|TodoListFactoryInterface
     */
    public function setTitle(Title $title): TodoListFactoryInterface
    {
        $this->title = (string)$title;

        return $this;
    }

    /**
     * @param DateTime $createdAt
     * @return $this|TodoListFactoryInterface
     */
    public function setCreatedAt(DateTime $createdAt): TodoListFactoryInterface
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @param DateTime $updatedAt
     * @return $this|TodoListFactoryInterface
     */
    public function setUpdatedAt(DateTime $updatedAt): TodoListFactoryInterface
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
