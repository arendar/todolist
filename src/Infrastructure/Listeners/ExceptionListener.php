<?php

namespace TodoList\Infrastructure\Listeners;

use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use TodoList\Application\Interfaces\ApiExceptionInterface;

/**
 * Class ExceptionListener
 * @package TodoList\Infrastructure\Listeners
 */
class ExceptionListener
{
    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getException();

        if (!$event->getException() instanceof ApiExceptionInterface) {
            return;
        }

        $event->setException(new HttpException($exception->getResponseCode(), $exception->getMessage()));
    }
}
