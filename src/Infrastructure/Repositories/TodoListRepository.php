<?php

namespace TodoList\Infrastructure\Repositories;

use TodoList\Application\Interfaces\TodoList\TodoListRepositoryInterface;
use TodoList\Domain\Entities\TodoList;
use TodoList\Domain\ValueObjects\Title;
use TodoList\Infrastructure\DoctrineEntity\Domain\TodoList as DoctrineTodoList;

/**
 * Class TodoListRepository
 * @package TodoList\Infrastructure\Repositories
 */
class TodoListRepository extends AbstractRepository implements TodoListRepositoryInterface
{
    /**
     * @param int $id
     * @return TodoList|null|mixed
     */
    public function find(int $id): ?TodoList
    {
        return $this->getEntityManager()
            ->getRepository(DoctrineTodoList::class)
            ->find($id);
    }

    /**
     * @param int $listId
     * @param int $userId
     * @return TodoList|null|mixed
     */
    public function getByListIdUserId(int $listId, int $userId): ?TodoList
    {
        return $this->getEntityManager()
            ->getRepository(DoctrineTodoList::class)
            ->findOneBy(['userId' => $userId, 'listId' => $listId]);
    }

    /**
     * @param int $userId
     * @return TodoList[]
     */
    public function getByUserId(int $userId): array
    {
        return $this->getEntityManager()
            ->getRepository(DoctrineTodoList::class)
            ->findBy(['userId' => $userId]);
    }

    /**
     * @param TodoList $item
     * @return TodoList
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(TodoList $item): TodoList
    {
        if (!($item instanceof DoctrineTodoList)) {
            throw new \LogicException('Exception instanceof');
        }

        $this->getEntityManager()->persist($item);
        $this->getEntityManager()->flush();

        return $item;
    }

    /**
     * @param TodoList $item
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(TodoList $item): void
    {
        if (!($item instanceof DoctrineTodoList)) {
            throw new \LogicException('Exception instanceof');
        }

        $this->getEntityManager()->remove($item);
        $this->getEntityManager()->flush();
    }

    /**
     * @param int $userId
     * @param Title $title
     * @return TodoList|null|mixed
     */
    public function getByTitle(int $userId, Title $title): ?TodoList
    {
        return $this->getEntityManager()
            ->getRepository(DoctrineTodoList::class)
            ->findOneBy(['userId' => $userId, 'title' => (string)$title]);
    }

    /**
     * @param int $id
     * @param Title $title
     */
    public function updateTitle(int $id, Title $title): void
    {
        $entity = $this->find($id);

        $entity->setTitle($title);
        $entity->setUpdatedAt(new \DateTime());

        $this->getEntityManager()->flush();
    }
}
