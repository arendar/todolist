<?php

namespace TodoList\Infrastructure\Repositories;

use TodoList\Application\Interfaces\TodoItem\TodoItemRepositoryInterface;
use TodoList\Domain\Entities\TodoItem;
use TodoList\Domain\ValueObjects\Status;
use TodoList\Domain\ValueObjects\Title;
use TodoList\Infrastructure\DoctrineEntity\Domain\TodoItem as DoctrineTodoItem;

/**
 * Class TodoItemRepository
 * @package TodoList\Infrastructure\Repositories
 */
class TodoItemRepository extends AbstractRepository implements TodoItemRepositoryInterface
{
    /**
     * @param int $id
     * @return TodoItem|null|mixed
     */
    public function find(int $id): ?TodoItem
    {
        return $this->getEntityManager()
            ->getRepository(DoctrineTodoItem::class)
            ->find($id);
    }

    /**
     * @param int $listId
     * @return TodoItem[]
     */
    public function getByListId(int $listId): array
    {
        return $this->getEntityManager()
            ->getRepository(DoctrineTodoItem::class)
            ->findBy(['listId' => $listId]);
    }

    /**
     * @param int $listId
     * @param Title $title
     * @return TodoItem|null|mixed
     */
    public function getByTitle(int $listId, Title $title): ?TodoItem
    {
        return $this->getEntityManager()
            ->getRepository(DoctrineTodoItem::class)
            ->findOneBy(['listId' => $listId, 'title' => $title]);
    }

    /**
     * @param int $listId
     * @param Status $status
     * @return TodoItem[]
     */
    public function getByStatus(int $listId, Status $status): array
    {
        return $this->getEntityManager()
            ->getRepository(DoctrineTodoItem::class)
            ->findBy(['status' => (string)$status]);
    }

    /**
     * @param TodoItem $item
     * @return TodoItem
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(TodoItem $item): TodoItem
    {
        if (!($item instanceof DoctrineTodoItem)) {
            throw new \LogicException('Exception instanceof');
        }

        $this->getEntityManager()->persist($item);
        $this->getEntityManager()->flush();

        return $item;
    }

    /**
     * @param int $id
     * @param TodoItem $item
     * @return TodoItem
     */
    public function update(int $id, TodoItem $item): TodoItem
    {
        $entity = $this->find($id);

        $entity->setTitle($item->getTitle());
        $entity->setNote($item->getNote());
        $entity->setStatus($item->getStatus());
        $entity->setUpdatedAt(new \DateTime());

        $this->getEntityManager()->flush();

        return $entity;
    }

    /**
     * @param TodoItem $item
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(TodoItem $item): void
    {
        if (!($item instanceof DoctrineTodoItem)) {
            throw new \LogicException('Exception instanceof');
        }

        $this->getEntityManager()->remove($item);
        $this->getEntityManager()->flush();
    }
}
