<?php

namespace TodoList\Infrastructure\Repositories;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Class AbstractRepository
 * @package TodoList\Infrastructure\Repositories
 */
class AbstractRepository
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * AbstractRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }
}
