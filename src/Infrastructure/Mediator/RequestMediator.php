<?php

namespace TodoList\Infrastructure\Mediator;

use Symfony\Component\DependencyInjection\ContainerInterface;
use TodoList\Application\Common\Interfaces\RequestHandler;
use TodoList\Application\Common\Mediator\AbstractMediator;

/**
 * Class Mediator
 * @package TodoList\Infrastructure\Mediator
 */
class RequestMediator extends AbstractMediator
{
    /**
     * @var ContainerInterface
     */
    private ContainerInterface $builder;

    /**
     * Mediator constructor.
     * @param ContainerInterface $builder
     */
    public function __construct(ContainerInterface $builder)
    {
        $this->builder = $builder;
    }

    /**
     * @param string $handlerName
     * @return RequestHandler|mixed
     * @throws \Exception
     */
    protected function getHandler(string $handlerName): RequestHandler
    {
        return $this->builder->get($handlerName);
    }
}
