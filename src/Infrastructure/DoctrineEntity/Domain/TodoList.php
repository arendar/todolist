<?php

namespace TodoList\Infrastructure\DoctrineEntity\Domain;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use TodoList\Domain\Entities\TodoList as DomainTodoList;

/**
 * @ORM\Entity
 * @ORM\Table(name="todo_lists", indexes={@ORM\Index(name="user_id_idx", columns={"user_id"})})
 */
class TodoList extends DomainTodoList
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected int $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="user_id", nullable=false)
     */
    protected int $userId;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected string $title;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime")
     */
    protected DateTime $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime")
     */
    protected DateTime $updatedAt;
}
