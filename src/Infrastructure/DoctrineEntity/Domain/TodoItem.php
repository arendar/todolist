<?php

namespace TodoList\Infrastructure\DoctrineEntity\Domain;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use TodoList\Domain\Entities\TodoItem as DomainTodoItem;

/**
 * @ORM\Entity
 * @ORM\Table(name="todo_items", indexes={@ORM\Index(name="list_id_idx", columns={"list_id"})})
 */
class TodoItem extends DomainTodoItem
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected int $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="list_id", nullable=false)
     */
    protected int $listId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    protected string $status;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    protected string $title;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected ?string $note;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime")
     */
    protected DateTime $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime")
     */
    protected DateTime $updatedAt;
}
