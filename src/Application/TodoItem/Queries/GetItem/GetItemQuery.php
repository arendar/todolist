<?php

namespace TodoList\Application\TodoItem\Queries\GetItem;

use TodoList\Application\Common\Interfaces\Validators\TodoItemValidatorContract;
use TodoList\Application\Common\Request\AbstractRequest;

/**
 * Class GetItemQuery
 * @package TodoList\Application\TodoItem\Queries\GetItem
 */
class GetItemQuery extends AbstractRequest
{
    /**
     * @var TodoItemValidatorContract
     */
    private TodoItemValidatorContract $itemValidator;

    /**
     * CreateTodoItemCommand constructor.
     * @param TodoItemValidatorContract $itemValidator
     */
    public function __construct(TodoItemValidatorContract $itemValidator)
    {
        $this->itemValidator = $itemValidator;
    }

    public function validateRequest(): void
    {
        $this->itemValidator->checkNote($this->getUserId(), $this->getParameters()['note_id'] ?? 0);
    }
}
