<?php

namespace TodoList\Application\TodoItem\Queries\GetItem;

use TodoList\Application\Common\Handler\AbstractRequestHandler;
use TodoList\Application\Common\Interfaces\Request;
use TodoList\Application\Common\Interfaces\Response;
use TodoList\Application\Interfaces\TodoItem\TodoItemRepositoryInterface;

/**
 * Class GetItemQueryHandler
 * @package TodoList\Application\TodoItem\Queries\GetItem
 */
class GetItemQueryHandler extends AbstractRequestHandler
{
    /**
     * @var TodoItemRepositoryInterface
     */
    private TodoItemRepositoryInterface $repository;

    /**
     * GetItemQueryHandler constructor.
     * @param Response $response
     * @param TodoItemRepositoryInterface $repository
     */
    public function __construct(Response $response, TodoItemRepositoryInterface $repository)
    {
        parent::__construct($response);
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request): Response
    {
        $request->validateRequest();

        $note = $this->repository->find($request->getParameters()['note_id']);

        return $this->response
            ->setMessage($note->toArray())
            ->setStatus(Response::SUCCESS);
    }
}
