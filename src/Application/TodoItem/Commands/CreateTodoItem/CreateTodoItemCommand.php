<?php

namespace TodoList\Application\TodoItem\Commands\CreateTodoItem;

use TodoList\Application\Common\Interfaces\Validators\TodoItemValidatorContract;
use TodoList\Application\Common\Request\AbstractRequest;

/**
 * Class CreateTodoItemCommand
 * @package TodoList\Application\TodoItem\Commands
 */
class CreateTodoItemCommand extends AbstractRequest
{
    /**
     * @var TodoItemValidatorContract
     */
    private TodoItemValidatorContract $itemValidator;

    /**
     * CreateTodoItemCommand constructor.
     * @param TodoItemValidatorContract $itemValidator
     */
    public function __construct(TodoItemValidatorContract $itemValidator)
    {
        $this->itemValidator = $itemValidator;
    }

    public function validateRequest(): void
    {
        $listId = $this->getParameters()['list_id'] ?? 0;
        $title = $this->getParameters()['title'] ?? '';

        $this->itemValidator
            ->checkTitle($title)
            ->checkListId($listId)
            ->checkListIdExists($listId)
            ->checkPermission($listId, $this->getUserId())
            ->checkForDuplicateTitle($listId, $title)
            ->checkStatus($this->getParameters()['status'] ?? '');
    }
}
