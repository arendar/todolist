<?php

namespace TodoList\Application\TodoItem\Commands\CreateTodoItem;

use TodoList\Application\Common\Handler\AbstractRequestHandler;
use TodoList\Application\Common\Interfaces\Request;
use TodoList\Application\Common\Interfaces\Response;
use TodoList\Application\Interfaces\TodoItem\TodoItemFactoryInterface;
use TodoList\Application\Interfaces\TodoItem\TodoItemRepositoryInterface;
use TodoList\Domain\Entities\TodoItem;
use TodoList\Domain\ValueObjects\Status;
use TodoList\Domain\ValueObjects\Title;

/**
 * Class CreateTodoItemCommandHandler
 * @package TodoList\Application\TodoItem\Commands
 */
class CreateTodoItemCommandHandler extends AbstractRequestHandler
{
    /**
     * @var TodoItemRepositoryInterface
     */
    private TodoItemRepositoryInterface $repository;

    /**
     * @var TodoItemFactoryInterface
     */
    private TodoItemFactoryInterface $factory;

    /**
     * CreateTodoItemCommandHandler constructor.
     * @param Response $response
     * @param TodoItemRepositoryInterface $repository
     * @param TodoItemFactoryInterface $factory
     */
    public function __construct(
        Response $response,
        TodoItemRepositoryInterface $repository,
        TodoItemFactoryInterface $factory
    ) {
        parent::__construct($response);
        $this->repository = $repository;
        $this->factory = $factory;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request): Response
    {
        $request->validateRequest();

        $item = $this->repository->save($this->getEntity($request));

        return $this->response
            ->setMessage($item->toArray())
            ->setStatus(Response::CREATED);
    }

    /**
     * @param Request $request
     * @return TodoItem
     */
    private function getEntity(Request $request): TodoItem
    {
        $parameters = $request->getParameters();

        return $this->factory
            ->setStatus(new Status($parameters['status']))
            ->setTitle(new Title($parameters['title']))
            ->setListId($parameters['list_id'])
            ->setNote($parameters['note'] ?? null)
            ->setCreatedAt(new \DateTime())
            ->setUpdatedAt(new \DateTime())
            ->make();
    }
}
