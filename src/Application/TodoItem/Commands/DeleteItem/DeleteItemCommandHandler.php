<?php

namespace TodoList\Application\TodoItem\Commands\DeleteItem;

use TodoList\Application\Common\Handler\AbstractRequestHandler;
use TodoList\Application\Common\Interfaces\Request;
use TodoList\Application\Common\Interfaces\Response;
use TodoList\Application\Interfaces\TodoItem\TodoItemRepositoryInterface;

/**
 * Class DeleteItemCommandHandler
 * @package TodoList\Application\TodoItem\Commands\DeleteItem
 */
class DeleteItemCommandHandler extends AbstractRequestHandler
{
    /**
     * @var TodoItemRepositoryInterface
     */
    private TodoItemRepositoryInterface $repository;

    /**
     * DeleteItemCommandHandler constructor.
     * @param Response $response
     * @param TodoItemRepositoryInterface $repository
     */
    public function __construct(Response $response, TodoItemRepositoryInterface $repository)
    {
        parent::__construct($response);
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request): Response
    {
        $request->validateRequest();

        $this->repository->delete($this->repository->find($request->getParameters()['note_id']));

        return $this->response->setStatus(Response::NO_CONTENT);
    }
}
