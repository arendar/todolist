<?php

namespace TodoList\Application\TodoItem\Commands\DeleteItem;

use TodoList\Application\Common\Interfaces\Validators\TodoItemValidatorContract;
use TodoList\Application\Common\Request\AbstractRequest;

/**
 * Class DeleteItemCommand
 * @package TodoList\Application\TodoItem\Commands\DeleteItem
 */
class DeleteItemCommand extends AbstractRequest
{
    /**
     * @var TodoItemValidatorContract
     */
    private TodoItemValidatorContract $itemValidator;

    /**
     * CreateTodoItemCommand constructor.
     * @param TodoItemValidatorContract $itemValidator
     */
    public function __construct(TodoItemValidatorContract $itemValidator)
    {
        $this->itemValidator = $itemValidator;
    }

    public function validateRequest(): void
    {
        $this->itemValidator->checkNote($this->getUserId(), $this->getParameters()['note_id'] ?? 0);
    }
}
