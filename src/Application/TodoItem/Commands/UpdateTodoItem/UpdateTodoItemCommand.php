<?php

namespace TodoList\Application\TodoItem\Commands\UpdateTodoItem;

use TodoList\Application\Common\Interfaces\Validators\TodoItemValidatorContract;
use TodoList\Application\Common\Request\AbstractRequest;
use TodoList\Application\Interfaces\TodoItem\TodoItemRepositoryInterface;

/**
 * Class UpdateTodoItemCommand
 * @package TodoList\Application\TodoItem\Commands
 */
class UpdateTodoItemCommand extends AbstractRequest
{
    /**
     * @var TodoItemValidatorContract
     */
    private TodoItemValidatorContract $itemValidator;

    /**
     * @var TodoItemRepositoryInterface
     */
    private TodoItemRepositoryInterface $itemRepository;

    /**
     * UpdateTodoItemCommand constructor.
     * @param TodoItemValidatorContract $itemValidator
     * @param TodoItemRepositoryInterface $itemRepository
     */
    public function __construct(TodoItemValidatorContract $itemValidator, TodoItemRepositoryInterface $itemRepository)
    {
        $this->itemValidator = $itemValidator;
        $this->itemRepository = $itemRepository;
    }

    public function validateRequest(): void
    {
        $noteId = $this->getParameters()['note_id'] ?? 0;
        $title = $this->getParameters()['title'] ?? '';

        $this->itemValidator
            ->checkTitle($title)
            ->checkStatus($this->getParameters()['status'] ?? '')
            ->checkNote($this->getUserId(), $noteId);

        $note = $this->itemRepository->find($noteId);

        $this->itemValidator->checkForDuplicateTitle($note->getListId(), $title);
    }
}
