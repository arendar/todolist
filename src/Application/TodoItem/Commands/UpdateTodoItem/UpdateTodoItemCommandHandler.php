<?php

namespace TodoList\Application\TodoItem\Commands\UpdateTodoItem;

use TodoList\Application\Common\Handler\AbstractRequestHandler;
use TodoList\Application\Common\Interfaces\Request;
use TodoList\Application\Common\Interfaces\Response;
use TodoList\Application\Interfaces\TodoItem\TodoItemFactoryInterface;
use TodoList\Application\Interfaces\TodoItem\TodoItemRepositoryInterface;
use TodoList\Domain\Entities\TodoItem;
use TodoList\Domain\ValueObjects\Status;
use TodoList\Domain\ValueObjects\Title;

/**
 * Class UpdateTodoItemCommandHandler
 * @package TodoList\Application\TodoItem\Commands\UpdateTodoItem
 */
class UpdateTodoItemCommandHandler extends AbstractRequestHandler
{
    /**
     * @var TodoItemRepositoryInterface
     */
    private TodoItemRepositoryInterface $repository;

    /**
     * @var TodoItemFactoryInterface
     */
    private TodoItemFactoryInterface $factory;

    /**
     * UpdateTodoItemCommandHandler constructor.
     * @param Response $response
     * @param TodoItemRepositoryInterface $repository
     * @param TodoItemFactoryInterface $factory
     */
    public function __construct(
        Response $response,
        TodoItemRepositoryInterface $repository,
        TodoItemFactoryInterface $factory
    ) {
        parent::__construct($response);
        $this->repository = $repository;
        $this->factory = $factory;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request): Response
    {
        $request->validateRequest();

        $this->repository->update($request->getParameters()['note_id'], $this->getEntity($request));

        return $this->response->setStatus(Response::NO_CONTENT);
    }

    /**
     * @param Request $request
     * @return TodoItem
     */
    private function getEntity(Request $request): TodoItem
    {
        $parameters = $request->getParameters();

        if (!empty($parameters['status'])) {
            $this->factory->setStatus(new Status($parameters['status']));
        }

        if (!empty($parameters['title'])) {
            $this->factory->setTitle(new Title($parameters['title']));
        }

        if (!empty($parameters['note'])) {
            $this->factory ->setNote($parameters['note']);
        }


        return $this->factory
            ->setId($parameters['note_id'])
            ->setUpdatedAt(new \DateTime())
            ->make();
    }
}
