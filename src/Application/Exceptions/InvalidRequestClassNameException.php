<?php

namespace TodoList\Application\Exceptions;

/**
 * Class InvalidClassNameException
 * @package TodoList\Application\Exceptions
 */
class InvalidRequestClassNameException extends \Exception
{

}
