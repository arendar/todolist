<?php

namespace TodoList\Application\Exceptions;

use TodoList\Application\Interfaces\ApiExceptionInterface;

/**
 * Class ApiException
 * @package TodoList\Application\Exceptions
 */
class ApiException extends \Exception implements ApiExceptionInterface
{
    /**
     * @var int
     */
    private int $responseCode;

    /**
     * @return int
     */
    public function getResponseCode(): int
    {
        return $this->responseCode;
    }

    /**
     * @param int $code
     * @return $this
     */
    public function setResponseCode(int $code): self
    {
        $this->responseCode = $code;
        return $this;
    }
}
