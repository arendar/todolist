<?php

namespace TodoList\Application\Exceptions;

/**
 * Class HandlerClassDoesNotExistException
 * @package TodoList\Application\Exceptions
 */
class HandlerNotRegisteredException extends \Exception
{

}
