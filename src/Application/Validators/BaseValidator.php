<?php

namespace TodoList\Application\Validators;

use TodoList\Application\Common\Interfaces\Response;
use TodoList\Application\Common\Interfaces\Validators\Validator;
use TodoList\Application\Exceptions\ApiException;
use TodoList\Application\Interfaces\TodoList\TodoListRepositoryInterface;
use TodoList\Domain\Exceptions\InvalidTitleException;
use TodoList\Domain\ValueObjects\Title;

/**
 * Class BaseValidator
 * @package TodoList\Application\Common\Validators
 */
abstract class BaseValidator implements Validator
{
    /**
     * @var TodoListRepositoryInterface
     */
    protected TodoListRepositoryInterface $listRepository;

    /**
     * BaseValidator constructor.
     * @param TodoListRepositoryInterface $listRepository
     */
    public function __construct(TodoListRepositoryInterface $listRepository)
    {
        $this->listRepository = $listRepository;
    }

    /**
     * @param int $listId
     * @return $this
     * @throws ApiException
     */
    public function checkListId(int $listId): self
    {
        if (empty($listId)) {
            throw (new ApiException('Parameter list_id must not be empty.'))->setResponseCode(Response::BAD_REQUEST);
        }

        return $this;
    }

    /**
     * @param int $listId
     * @return $this
     * @throws ApiException
     */
    public function checkListIdExists(int $listId): self
    {
        $list = $this->listRepository->find($listId);

        if ($list === null) {
            throw (new ApiException("List with id=$listId not found."))->setResponseCode(Response::NOT_FOUND);
        }

        return $this;
    }

    /**
     * @param int $listId
     * @param int $userId
     * @return $this
     * @throws ApiException
     */
    public function checkPermission(int $listId, int $userId): self
    {
        $list = $this->listRepository->find($listId);

        if ($list !== null && $list->getUserId() !== $userId) {
            throw (new ApiException("You don't have permissions to this list."))->setResponseCode(Response::FORBIDDEN);
        }

        return $this;
    }

    /**
     * @param string $title
     * @return $this
     * @throws ApiException
     */
    public function checkTitle(string $title): self
    {
        try {
            new Title($title);
        } catch (InvalidTitleException $exception) {
            throw (new ApiException($exception->getMessage()))->setResponseCode(Response::BAD_REQUEST);
        }

        return $this;
    }
}
