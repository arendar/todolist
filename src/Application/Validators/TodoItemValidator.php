<?php

namespace TodoList\Application\Validators;

use TodoList\Application\Common\Interfaces\Response;
use TodoList\Application\Common\Interfaces\Validators\TodoItemValidatorContract;
use TodoList\Application\Exceptions\ApiException;
use TodoList\Application\Interfaces\TodoItem\TodoItemRepositoryInterface;
use TodoList\Application\Interfaces\TodoList\TodoListRepositoryInterface;
use TodoList\Domain\Exceptions\InvalidStatusException;
use TodoList\Domain\ValueObjects\Status;
use TodoList\Domain\ValueObjects\Title;

/**
 * Class TodoItemValidator
 * @package TodoList\Application\Common\Validators
 */
class TodoItemValidator extends BaseValidator implements TodoItemValidatorContract
{
    /**
     * @var TodoItemRepositoryInterface
     */
    private TodoItemRepositoryInterface $itemRepository;

    /**
     * TodoItemValidator constructor.
     * @param TodoListRepositoryInterface $listRepository
     * @param TodoItemRepositoryInterface $itemRepository
     */
    public function __construct(
        TodoListRepositoryInterface $listRepository,
        TodoItemRepositoryInterface $itemRepository
    ) {
        parent::__construct($listRepository);
        $this->itemRepository = $itemRepository;
    }

    /**
     * @param int $listId
     * @param string $title
     * @return $this
     * @throws ApiException
     */
    public function checkForDuplicateTitle(int $listId, string $title): TodoItemValidator
    {
        if ($this->itemRepository->getByTitle($listId, new Title($title)) !== null) {
            throw (new ApiException('Task with the same title already exists.'))->setResponseCode(Response::CONFLICT);
        }

        return $this;
    }

    /**
     * @param int $userId
     * @param int $noteId
     * @return $this
     * @throws ApiException
     */
    public function checkNote(int $userId, int $noteId): TodoItemValidator
    {
        if (empty($noteId)) {
            throw (new ApiException('Parameter note_id must not be empty.'))->setResponseCode(Response::BAD_REQUEST);
        }

        $note = $this->itemRepository->find($noteId);

        if ($note === null) {
            throw (new ApiException("Note with id=$noteId not found."))->setResponseCode(Response::NOT_FOUND);
        }

        $this->checkPermission($note->getListId(), $userId);

        return $this;
    }

    /**
     * @param int $noteId
     * @return $this
     * @throws ApiException
     */
    public function checkNoteIdExists(int $noteId): TodoItemValidator
    {
        $note = $this->itemRepository->find($noteId);

        if ($note === null) {
            throw (new ApiException("Note with id=$noteId not found."))->setResponseCode(Response::NOT_FOUND);
        }

        return $this;
    }

    /**
     * @param string $status
     * @return $this
     * @throws ApiException
     */
    public function checkStatus(string $status): self
    {
        try {
            new Status($status);
        } catch (InvalidStatusException $exception) {
            throw (new ApiException($exception->getMessage()))->setResponseCode(Response::BAD_REQUEST);
        }

        return $this;
    }
}
