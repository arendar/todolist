<?php

namespace TodoList\Application\Validators;

use TodoList\Application\Common\Interfaces\Response;
use TodoList\Application\Common\Interfaces\Validators\TodoListValidatorContract;
use TodoList\Application\Exceptions\ApiException;
use TodoList\Domain\ValueObjects\Title;

/**
 * Class TodoListValidator
 * @package TodoList\Application\Common\Validators
 */
class TodoListValidator extends BaseValidator implements TodoListValidatorContract
{
    /**
     * @param int $userId
     * @param string $title
     * @return $this
     * @throws ApiException
     */
    public function checkForDuplicateTitle(int $userId, string $title): TodoListValidator
    {
        if ($this->listRepository->getByTitle($userId, new Title($title)) !== null) {
            throw (new ApiException('List with the same title already exists.'))->setResponseCode(Response::CONFLICT);
        }

        return $this;
    }
}
