<?php

namespace TodoList\Application\Common\Interfaces;

/**
 * Interface RequestHandler
 * @package TodoList\Application\Interfaces
 */
interface RequestHandler
{
    /**
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request): Response;
}
