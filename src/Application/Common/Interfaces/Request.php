<?php

namespace TodoList\Application\Common\Interfaces;

/**
 * Interface Request
 * @package TodoList\Application\Interfaces
 */
interface Request
{
    /**
     * @return mixed
     */
    public function getUserId();

    /**
     * @param int $userId
     * @return void
     */
    public function setUserId(int $userId): void;

    /**
     * @return array
     */
    public function getParameters(): array;

    /**
     * @param $parameters
     * @return void
     */
    public function setParameters(array $parameters): void;

    public function validateRequest(): void;
}
