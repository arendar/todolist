<?php

namespace TodoList\Application\Common\Interfaces;

/**
 * Interface Response
 * @package TodoList\Application\Interfaces
 */
interface Response
{
    public const BAD_REQUEST = 400;
    public const FORBIDDEN = 403;
    public const NOT_FOUND = 404;
    public const CONFLICT = 409;
    public const SUCCESS = 200;
    public const CREATED = 201;
    public const NO_CONTENT = 204;

    /**
     * @return array|null
     */
    public function getMessage(): ?array;

    /**
     * @param array|null $message
     * @return self
     */
    public function setMessage(?array $message): self;

    /**
     * @return int
     */
    public function getStatus(): int;

    /**
     * @param int $status
     * @return self
     */
    public function setStatus(int $status): self;
}
