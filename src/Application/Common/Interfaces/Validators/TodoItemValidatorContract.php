<?php

namespace TodoList\Application\Common\Interfaces\Validators;

/**
 * Class TodoItemValidator
 * @package TodoList\Application\Common\Interfaces\Validators
 */
interface TodoItemValidatorContract extends Validator
{
    /**
     * @param string $status
     * @return $this
     */
    public function checkStatus(string $status): self;

    /**
     * @param int $userId
     * @param int $noteId
     * @return $this
     */
    public function checkNote(int $userId, int $noteId): self;

    /**
     * @param int $noteId
     * @return $this
     */
    public function checkNoteIdExists(int $noteId): self;

    /**
     * @param int $listId
     * @param string $title
     * @return $this
     */
    public function checkForDuplicateTitle(int $listId, string $title): self;
}
