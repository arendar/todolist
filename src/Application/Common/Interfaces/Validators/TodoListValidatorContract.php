<?php

namespace TodoList\Application\Common\Interfaces\Validators;

/**
 * Interface TodoListValidator
 * @package TodoList\Application\Common\Interfaces\Validators
 */
interface TodoListValidatorContract extends Validator
{
    /**
     * @param int $userId
     * @param string $title
     * @return $this
     */
    public function checkForDuplicateTitle(int $userId, string $title): self;
}
