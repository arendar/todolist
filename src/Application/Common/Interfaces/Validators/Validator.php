<?php

namespace TodoList\Application\Common\Interfaces\Validators;

/**
 * Interface Validator
 * @package TodoList\Application\Common\Interfaces\Validators
 */
interface Validator
{
    /**
     * @param int $listId
     */
    public function checkListId(int $listId);

    /**
     * @param int $listId
     */
    public function checkListIdExists(int $listId);

    /**
     * @param int $listId
     * @param int $userId
     */
    public function checkPermission(int $listId, int $userId);

    /**
     * @param string $title
     */
    public function checkTitle(string $title);
}
