<?php

namespace TodoList\Application\Common\Request;

use TodoList\Application\Common\Interfaces\Request;

/**
 * Class AbstractRequest
 * @package TodoList\Application\Common\Request
 */
abstract class AbstractRequest implements Request
{
    /**
     * @var int
     */
    private int $userId;

    /**
     * @var array
     */
    private array $parameters;

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param array $parameters
     */
    public function setParameters(array $parameters): void
    {
        $this->parameters = $parameters;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    abstract public function validateRequest(): void;
}
