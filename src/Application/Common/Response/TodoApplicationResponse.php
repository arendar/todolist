<?php

namespace TodoList\Application\Common\Response;

use TodoList\Application\Common\Interfaces\Response;

/**
 * Class TodoApplicationResponse
 * @package TodoList\Application\Common\Response
 */
class TodoApplicationResponse implements Response
{
    /**
     * @var int
     */
    private int $status;

    /**
     * @var array|null
     */
    private ?array $message = null;

    /**
     * @param int $status
     * @return TodoApplicationResponse
     */
    public function setStatus(int $status): TodoApplicationResponse
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param array|null $message
     * @return TodoApplicationResponse
     */
    public function setMessage(?array $message): TodoApplicationResponse
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getMessage(): ?array
    {
        return $this->message;
    }
}
