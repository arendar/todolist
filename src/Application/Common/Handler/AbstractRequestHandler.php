<?php

namespace TodoList\Application\Common\Handler;

use TodoList\Application\Common\Interfaces\Request;
use TodoList\Application\Common\Interfaces\RequestHandler;
use TodoList\Application\Common\Interfaces\Response;

/**
 * Class AbstractRequestHandler
 * @package TodoList\Application\Common\Handler
 */
abstract class AbstractRequestHandler implements RequestHandler
{
    /**
     * @var Response
     */
    protected Response $response;

    /**
     * AbstractRequestHandler constructor.
     * @param Response $response
     */
    public function __construct(Response $response) {
        $this->response = $response;
    }

    /**
     * @param Request $request
     * @return Response
     */
    abstract public function handle(Request $request): Response;
}
