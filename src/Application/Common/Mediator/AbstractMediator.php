<?php

namespace TodoList\Application\Common\Mediator;

use TodoList\Application\Common\Interfaces\Request;
use TodoList\Application\Common\Interfaces\RequestHandler;
use TodoList\Application\Common\Interfaces\Response;
use TodoList\Application\Exceptions\HandlerNotRegisteredException;
use TodoList\Application\Exceptions\InvalidRequestClassNameException;
use TodoList\Application\Interfaces\Mediator;

/**
 * Class RequestMediator
 * @package TodoList\Application\Mediator
 */
abstract class AbstractMediator implements Mediator
{
    private const CHECK_NAME_REGEX = '/Query|Command$/';
    private const HANDLER_NAME_PART = 'Handler';

    /**
     * @param Request $request
     * @return Response
     * @throws HandlerNotRegisteredException
     * @throws InvalidRequestClassNameException
     * @throws \ReflectionException
     */
    public function send(Request $request): Response
    {
        $requestClassName = $this->getRequestClassName($request);

        return $this->getHandler($this->getHandlerName($requestClassName))->handle($request);
    }

    /**
     * @param string $handlerName
     * @return RequestHandler
     */
    abstract protected function getHandler(string $handlerName): RequestHandler;

    /**
     * @param string $requestClassName
     * @return string
     * @throws HandlerNotRegisteredException
     */
    protected function getHandlerName(string $requestClassName): string
    {
        $handlerName = $requestClassName . self::HANDLER_NAME_PART;

        $this->assertHandlerNotRegistered($handlerName);

        return $handlerName;
    }

    /**
     * @param Request $request
     * @return string
     * @throws InvalidRequestClassNameException
     * @throws \ReflectionException
     */
    private function getRequestClassName(Request $request): string
    {
        $reflection = new \ReflectionClass($request);
        $className = $reflection->getName();

        $this->assertClassNameNotValid($className);

        return $className;
    }

    /**
     * @param string $className
     * @throws InvalidRequestClassNameException
     */
    private function assertClassNameNotValid(string $className): void
    {
        if (preg_match(self::CHECK_NAME_REGEX, $className) === 0) {
            throw new InvalidRequestClassNameException('The provided request name is invalid. Command must have "Command" or "Query" in the end!');
        }
    }

    /**
     * @param string $handlerName
     * @throws HandlerNotRegisteredException
     */
    private function assertHandlerNotRegistered(string $handlerName): void
    {
        if (!class_exists($handlerName)) {
            throw new HandlerNotRegisteredException("The request handler class $handlerName does not exist");
        }
    }
}
