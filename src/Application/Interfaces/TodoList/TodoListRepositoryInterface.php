<?php

namespace TodoList\Application\Interfaces\TodoList;

use TodoList\Domain\Entities\TodoList;
use TodoList\Domain\ValueObjects\Title;

/**
 * Interface TodoListRepositoryInterface
 * @package TodoList\Application\Interfaces\TodoLists
 */
interface TodoListRepositoryInterface
{
    /**
     * @param int $id
     * @return TodoList|null
     */
    public function find(int $id): ?TodoList;

    /**
     * @param int $listId
     * @param int $userId
     * @return TodoList|null
     */
    public function getByListIdUserId(int $listId, int $userId): ?TodoList;

    /**
     * @param int $userId
     * @return array
     */
    public function getByUserId(int $userId): array;

    /**
     * @param TodoList $item
     * @return TodoList
     */
    public function save(TodoList $item): TodoList;

    /**
     * @param TodoList $item
     */
    public function delete(TodoList $item): void;

    /**
     * @param int $userId
     * @param Title $title
     * @return TodoList|null
     */
    public function getByTitle(int $userId, Title $title): ?TodoList;

    /**
     * @param int $id
     * @param Title $title
     */
    public function updateTitle(int $id, Title $title): void;
}
