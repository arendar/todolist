<?php

namespace TodoList\Application\Interfaces\TodoList;

use DateTime;
use TodoList\Domain\Entities\TodoList;
use TodoList\Domain\ValueObjects\Title;

/**
 * Interface TodoListFactoryInterface
 * @package TodoList\Application\Interfaces\TodoLists
 */
interface TodoListFactoryInterface
{
    /**
     * @return TodoList
     */
    public function make(): TodoList;

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): TodoListFactoryInterface;

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId(int $userId): TodoListFactoryInterface;

    /**
     * @param Title $title
     * @return $this
     */
    public function setTitle(Title $title): TodoListFactoryInterface;

    /**
     * @param DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt(DateTime $createdAt): TodoListFactoryInterface;

    /**
     * @param DateTime $updatedAt
     * @return $this
     */
    public function setUpdatedAt(DateTime $updatedAt): TodoListFactoryInterface;
}
