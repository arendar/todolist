<?php

namespace TodoList\Application\Interfaces;

/**
 * Interface ApiExceptionInterface
 * @package TodoList\Application\Interfaces
 */
interface ApiExceptionInterface
{
    /**
     * @param int $code
     * @return $this
     */
    public function setResponseCode(int $code): self;

    /**
     * @return int
     */
    public function getResponseCode(): int;
}
