<?php

namespace TodoList\Application\Interfaces;

use TodoList\Application\Common\Interfaces\Request;
use TodoList\Application\Common\Interfaces\Response;

/**
 * Interface Mediator
 * @package TodoList\Application\Interfaces
 */
interface Mediator
{
    /**
     * @param Request $request
     * @return Response
     */
    public function send(Request $request): Response;
}
