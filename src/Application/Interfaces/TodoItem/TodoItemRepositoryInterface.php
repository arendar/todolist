<?php

namespace TodoList\Application\Interfaces\TodoItem;

use TodoList\Domain\Entities\TodoItem;
use TodoList\Domain\ValueObjects\Status;
use TodoList\Domain\ValueObjects\Title;

/**
 * Interface TodoItemRepositoryInterface
 * @package TodoList\Application\Interfaces\TodoItems
 */
interface TodoItemRepositoryInterface
{
    /**
     * @param int $id
     * @return TodoItem|null
     */
    public function find(int $id): ?TodoItem;

    /**
     * @param int $listId
     * @return array
     */
    public function getByListId(int $listId): array;

    /**
     * @param int $listId
     * @param Title $title
     * @return TodoItem|null
     */
    public function getByTitle(int $listId, Title $title): ?TodoItem;

    /**
     * @param int $listId
     * @param Status $status
     * @return array
     */
    public function getByStatus(int $listId, Status $status): array;

    /**
     * @param TodoItem $item
     * @return TodoItem
     */
    public function save(TodoItem $item): TodoItem;

    /**
     * @param int $id
     * @param TodoItem $item
     * @return TodoItem
     */
    public function update(int $id, TodoItem $item): TodoItem;

    /**
     * @param TodoItem $item
     */
    public function delete(TodoItem $item): void;
}
