<?php

namespace TodoList\Application\Interfaces\TodoItem;

use DateTime;
use TodoList\Domain\Entities\TodoItem;
use TodoList\Domain\ValueObjects\Status;
use TodoList\Domain\ValueObjects\Title;

/**
 * Interface TodoItemFactoryInterface
 * @package TodoList\Application\Interfaces\TodoItems
 */
interface TodoItemFactoryInterface
{
    /**
     * @return TodoItem
     */
    public function make(): TodoItem;

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): TodoItemFactoryInterface;

    /**
     * @param int $listId
     * @return TodoItemFactoryInterface
     */
    public function setListId(int $listId): TodoItemFactoryInterface;

    /**
     * @param Title $title
     * @return $this
     */
    public function setTitle(Title $title): TodoItemFactoryInterface;

    /**
     * @param Status $status
     * @return $this
     */
    public function setStatus(Status $status): TodoItemFactoryInterface;

    /**
     * @param string|null $note
     * @return TodoItemFactoryInterface
     */
    public function setNote(?string $note): TodoItemFactoryInterface;

    /**
     * @param DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt(DateTime $createdAt): TodoItemFactoryInterface;

    /**
     * @param DateTime $updatedAt
     * @return $this
     */
    public function setUpdatedAt(DateTime $updatedAt): TodoItemFactoryInterface;
}
