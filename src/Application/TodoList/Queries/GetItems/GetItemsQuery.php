<?php

namespace TodoList\Application\TodoList\Queries\GetItems;

use TodoList\Application\Common\Interfaces\Validators\TodoListValidatorContract;
use TodoList\Application\Common\Request\AbstractRequest;

/**
 * Class GetListCommand
 * @package TodoList\Application\Validators\TodoList\Queries\GetList
 */
class GetItemsQuery extends AbstractRequest
{
    /**
     * @var TodoListValidatorContract
     */
    private TodoListValidatorContract $listValidator;

    /**
     * CreateListCommand constructor.
     * @param TodoListValidatorContract $listValidator
     */
    public function __construct(TodoListValidatorContract $listValidator)
    {
        $this->listValidator = $listValidator;
    }

    public function validateRequest(): void
    {
        $listId = $this->getParameters()['list_id'];

        $this->listValidator
            ->checkListId($listId)
            ->checkListIdExists($listId)
            ->checkPermission($listId, $this->getUserId());
    }
}
