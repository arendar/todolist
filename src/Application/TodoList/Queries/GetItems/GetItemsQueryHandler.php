<?php

namespace TodoList\Application\TodoList\Queries\GetItems;

use TodoList\Application\Common\Handler\AbstractRequestHandler;
use TodoList\Application\Common\Interfaces\Request;
use TodoList\Application\Common\Interfaces\Response;
use TodoList\Application\Interfaces\TodoItem\TodoItemRepositoryInterface;
use TodoList\Domain\Entities\TodoItem;

/**
 * Class GetListQueryHandler
 * @package TodoList\Application\TodoList\Queries\GetList
 */
class GetItemsQueryHandler extends AbstractRequestHandler
{
    /**
     * @var TodoItemRepositoryInterface
     */
    private TodoItemRepositoryInterface $repository;

    /**
     * GetItemsQueryHandler constructor.
     * @param Response $response
     * @param TodoItemRepositoryInterface $repository
     */
    public function __construct(Response $response, TodoItemRepositoryInterface $repository)
    {
        parent::__construct($response);
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request): Response
    {
        $request->validateRequest();

        $list = $this->repository->getByListId($request->getParameters()['list_id']);

        return $this->response
            ->setMessage($this->prepareResponseArray($list))
            ->setStatus(Response::SUCCESS);
    }

    /**
     * @param TodoItem[] $items
     * @return array
     */
    private function prepareResponseArray(array $items): array
    {
        foreach ($items as $key => $item) {
            $items[$key] = $item->toArray();
        }

        return $items;
    }
}
