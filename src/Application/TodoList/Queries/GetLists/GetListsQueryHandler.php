<?php

namespace TodoList\Application\TodoList\Queries\GetLists;

use TodoList\Application\Common\Handler\AbstractRequestHandler;
use TodoList\Application\Common\Interfaces\Request;
use TodoList\Application\Common\Interfaces\Response;
use TodoList\Application\Interfaces\TodoList\TodoListRepositoryInterface;
use TodoList\Domain\Entities\TodoList;

/**
 * Class GetListsQueryHandler
 * @package TodoList\Application\TodoList\Queries\GetLists
 */
class GetListsQueryHandler extends AbstractRequestHandler
{
    /**
     * @var TodoListRepositoryInterface
     */
    private TodoListRepositoryInterface $repository;

    /**
     * GetListsQueryHandler constructor.
     * @param Response $response
     * @param TodoListRepositoryInterface $repository
     */
    public function __construct(Response $response, TodoListRepositoryInterface $repository)
    {
        parent::__construct($response);
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request): Response
    {
        $lists = $this->repository->getByUserId($request->getUserId());

        return $this->response
            ->setMessage($this->prepareResponseArray($lists))
            ->setStatus(Response::SUCCESS);
    }

    /**
     * @param TodoList[] $lists
     * @return array
     */
    private function prepareResponseArray(array $lists): array
    {
        foreach ($lists as $key => $list) {
            $lists[$key] = $list->toArray();
        }

        return $lists;
    }
}
