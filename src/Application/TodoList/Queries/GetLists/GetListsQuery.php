<?php

namespace TodoList\Application\TodoList\Queries\GetLists;

use TodoList\Application\Common\Request\AbstractRequest;

/**
 * Class GetListsQuery
 * @package TodoList\Application\TodoList\Queries\GetLists
 */
class GetListsQuery extends AbstractRequest
{
    public function validateRequest(): void
    {

    }
}
