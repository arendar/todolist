<?php

namespace TodoList\Application\TodoList\Queries\GetList;

use TodoList\Application\Common\Handler\AbstractRequestHandler;
use TodoList\Application\Common\Interfaces\Request;
use TodoList\Application\Common\Interfaces\Response;
use TodoList\Application\Interfaces\TodoList\TodoListRepositoryInterface;

/**
 * Class GetListQueryHandler
 * @package TodoList\Application\TodoList\Queries\GetList
 */
class GetListQueryHandler extends AbstractRequestHandler
{
    /**
     * @var TodoListRepositoryInterface
     */
    private TodoListRepositoryInterface $repository;

    /**
     * GetListQueryHandler constructor.
     * @param Response $response
     * @param TodoListRepositoryInterface $repository
     */
    public function __construct(Response $response, TodoListRepositoryInterface $repository)
    {
        parent::__construct($response);
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request): Response
    {
        $request->validateRequest();

        $list = $this->repository->find($request->getParameters()['list_id']);

        return $this->response
            ->setMessage($list->toArray())
            ->setStatus(Response::SUCCESS);
    }
}
