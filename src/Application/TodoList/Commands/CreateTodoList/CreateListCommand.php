<?php

namespace TodoList\Application\TodoList\Commands\CreateTodoList;

use TodoList\Application\Common\Interfaces\Validators\TodoListValidatorContract;
use TodoList\Application\Common\Request\AbstractRequest;

/**
 * Class CreateListCommand
 * @package TodoList\Application\TodoLists\Commands\CreateList
 */
class CreateListCommand extends AbstractRequest
{
    /**
     * @var TodoListValidatorContract
     */
    private TodoListValidatorContract $listValidator;

    /**
     * CreateListCommand constructor.
     * @param TodoListValidatorContract $listValidator
     */
    public function __construct(TodoListValidatorContract $listValidator)
    {
        $this->listValidator = $listValidator;
    }

    public function validateRequest(): void
    {
        $title = $this->getParameters()['title'] ?? '';

        $this->listValidator
            ->checkTitle($title)
            ->checkForDuplicateTitle($this->getUserId(), $title);
    }
}
