<?php

namespace TodoList\Application\TodoList\Commands\CreateTodoList;

use TodoList\Application\Common\Handler\AbstractRequestHandler;
use TodoList\Application\Common\Interfaces\Request;
use TodoList\Application\Common\Interfaces\Response;
use TodoList\Application\Interfaces\TodoList\TodoListFactoryInterface;
use TodoList\Application\Interfaces\TodoList\TodoListRepositoryInterface;
use TodoList\Domain\Entities\TodoList;
use TodoList\Domain\ValueObjects\Title;

/**
 * Class CreateListCommandHandler
 * @package TodoList\Application\TodoLists\Commands\CreateList
 */
class CreateListCommandHandler extends AbstractRequestHandler
{
    /**
     * @var TodoListRepositoryInterface
     */
    protected TodoListRepositoryInterface $repository;

    /**
     * @var TodoListFactoryInterface
     */
    private TodoListFactoryInterface $factory;

    /**
     * CreateListCommandHandler constructor.
     * @param TodoListRepositoryInterface $repository
     * @param Response $response
     * @param TodoListFactoryInterface $factory
     */
    public function __construct(
        Response $response,
        TodoListRepositoryInterface $repository,
        TodoListFactoryInterface $factory
    ) {
        parent::__construct($response);
        $this->factory = $factory;
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request): Response
    {
        $request->validateRequest();

        $list = $this->repository->save($this->getEntity($request));

        return $this->response
            ->setStatus(Response::CREATED)
            ->setMessage($list->toArray());
    }

    /**
     * @param Request $request
     * @return TodoList
     */
    private function getEntity(Request $request): TodoList
    {
        return $this->factory
            ->setTitle(new Title($request->getParameters()['title']))
            ->setUserId($request->getUserId())
            ->setUpdatedAt(new \DateTime())
            ->setCreatedAt(new \DateTime())
            ->make();
    }
}
