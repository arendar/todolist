<?php

namespace TodoList\Application\TodoList\Commands\UpdateTitle;

use TodoList\Application\Common\Interfaces\Validators\TodoListValidatorContract;
use TodoList\Application\Common\Request\AbstractRequest;

/**
 * Class UpdateTitleCommand
 * @package TodoList\Application\TodoList\Commands\UpdateTitle
 */
class UpdateTitleCommand extends AbstractRequest
{
    /**
     * @var TodoListValidatorContract
     */
    private TodoListValidatorContract $listValidator;

    /**
     * CreateListCommand constructor.
     * @param TodoListValidatorContract $listValidator
     */
    public function __construct(TodoListValidatorContract $listValidator)
    {
        $this->listValidator = $listValidator;
    }

    public function validateRequest(): void
    {
        $listId = $this->getParameters()['list_id'];
        $title = $this->getParameters()['title'] ?? '';

        $this->listValidator
            ->checkListId($listId)
            ->checkListIdExists($listId)
            ->checkPermission($listId, $this->getUserId())
            ->checkTitle($title)
            ->checkForDuplicateTitle($this->getUserId(), $title);
    }
}
