<?php

namespace TodoList\Application\TodoList\Commands\UpdateTitle;

use TodoList\Application\Common\Handler\AbstractRequestHandler;
use TodoList\Application\Common\Interfaces\Request;
use TodoList\Application\Common\Interfaces\Response;
use TodoList\Application\Interfaces\TodoList\TodoListRepositoryInterface;
use TodoList\Domain\ValueObjects\Title;

/**
 * Class UpdateTitleCommandHandler
 * @package TodoList\Application\TodoList\Commands\UpdateTitle
 */
class UpdateTitleCommandHandler extends AbstractRequestHandler
{
    /**
     * @var TodoListRepositoryInterface
     */
    protected TodoListRepositoryInterface $repository;

    /**
     * CreateListCommandHandler constructor.
     * @param TodoListRepositoryInterface $repository
     * @param Response $response
     */
    public function __construct(
        Response $response,
        TodoListRepositoryInterface $repository
    ) {
        parent::__construct($response);
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request): Response
    {
        $request->validateRequest();

        $this->repository
            ->updateTitle($request->getParameters()['list_id'], new Title($request->getParameters()['title']));

        return $this->response->setStatus(Response::NO_CONTENT);
    }
}
