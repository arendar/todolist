<?php

namespace TodoList\Application\TodoList\Commands\DeleteList;

use TodoList\Application\Common\Handler\AbstractRequestHandler;
use TodoList\Application\Common\Interfaces\Request;
use TodoList\Application\Common\Interfaces\Response;
use TodoList\Application\Interfaces\TodoList\TodoListRepositoryInterface;
use TodoList\Domain\Entities\TodoList;

class DeleteListCommandHandler extends AbstractRequestHandler
{
    /**
     * @var TodoListRepositoryInterface
     */
    private TodoListRepositoryInterface $repository;

    /**
     * DeleteListCommandHandler constructor.
     * @param Response $response
     * @param TodoListRepositoryInterface $repository
     */
    public function __construct(Response $response, TodoListRepositoryInterface $repository)
    {
        parent::__construct($response);
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request): Response
    {
        $request->validateRequest();

        $this->repository->delete($this->getEntity($request));

        return $this->response->setStatus(Response::NO_CONTENT);
    }

    /**
     * @param Request $request
     * @return TodoList
     */
    protected function getEntity(Request $request): TodoList
    {
        return $this->repository->find($request->getParameters()['list_id']);
    }
}
