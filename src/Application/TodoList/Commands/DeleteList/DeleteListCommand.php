<?php

namespace TodoList\Application\TodoList\Commands\DeleteList;

use TodoList\Application\Common\Interfaces\Validators\TodoListValidatorContract;
use TodoList\Application\Common\Request\AbstractRequest;

/**
 * Class DeleteListCommand
 * @package TodoList\Application\TodoList\Commands\DeleteList
 */
class DeleteListCommand extends AbstractRequest
{
    /**
     * @var TodoListValidatorContract
     */
    private TodoListValidatorContract $listValidator;

    /**
     * CreateListCommand constructor.
     * @param TodoListValidatorContract $listValidator
     */
    public function __construct(TodoListValidatorContract $listValidator)
    {
        $this->listValidator = $listValidator;
    }

    public function validateRequest(): void
    {
        $listId = $this->getParameters()['list_id'];

        $this->listValidator
            ->checkListId($listId)
            ->checkListIdExists($listId)
            ->checkPermission($listId, $this->getUserId());
    }
}
