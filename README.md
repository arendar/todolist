# TODO List application
Created by Yriy Arendar

## Setup project
### Host
#### For linux 
Add `172.16.237.1 todolist.local` to your /etc/hosts file
#### For mac
Add `127.0.0.1 todolist.local` to your /etc/hosts file
#### For windows
You need to add a record to the into `C:\Windows\System32\drivers\etc\hosts` file with an IP address of your virtual machine usually it looks like `192.168.99.100 todolist.local` 

### Starting container

1. Copy `.env.example` to the project directory folder and rename it to `.env`
2. Run `docker-compose up -d` and wait until all dependencies are installing with composer
3. Run migrations with this command: `docker-compose exec php php bin/console doctrine:migrations:migrate -n`
4. For using an API interface for this application you have to create API client and user
5. Create a client using this command: `docker-compose exec php php bin/console fos:oauth-server:create-client --grant-type=password
`
You will see client id and client secret 
6. Create a user with this command: `docker-compose exec php php bin/console fos:user:create`. You will need to enter a username, email, and password.

If everything is ok you will see a welcome page by the link `http://todolist.local/`


Run tests command: `docker-compose exec php php vendor/bin/phpunit`

P.S. I did not have time to write all the tests

### API interface

#### 1. Get token
url: `http://todolist.local/oauth/v2/token`

method: `GET`

parameters: 

`client_id: 2_25qvtnrt7b0gs0cs4kc888804kockco884ccgowkwss040kg44`

`client_secret: 2hyu4sf3ts4kscowwogo8ok8k0gwwg0sccsk004sk804k84k40`

`response_type: token`

`grant_type: password`

`username: test`

`password: test`

success response status: `200 OK`


error response status: `400 bad request `


response example: 

`{
   "access_token": "M2EyMGJiNDgxNjliM2JkYzJlYzNhYTljZGY2YjQ2ZDIzMTIwNzM5M2NmYmZjM2Q1YTgzN2Q2MWJlMGEzN2ExZA",
   "expires_in": 86400,
   "token_type": "bearer",
   "scope": null,
   "refresh_token": "MmNiMWRjYmNmMmFkZDIxMjI3YmIyMTM2MzlmYWJhNGMyMDFmMDJiNmMyZGE1ZDY3NDUzZTMxYWVkY2RmNjIyZA"
 }`
 
#### 2. Create list 

url: `http://todolist.local/api/list`


method: `POST`

parameters:

`title: list name`

success status: `201 Created`


success response: 

`{
  "id": 2,
  "userId": 1,
  "title": "test",
  "createdAt": {
    "date": "2020-07-06 07:06:08.309284",
    "timezone_type": 3,
    "timezone": "UTC"
  },
  "updatedAt": {
    "date": "2020-07-06 07:06:08.309276",
    "timezone_type": 3,
    "timezone": "UTC"
  }
}`

error response status: `401 Unauthorized`, `400 bad request`, `409 conflict`

#### 3. Update list title 

url: `http://todolist.local/api/list/{listId}/update-title`

method: `PATCH`

parameters:

`title: list name`

success status: `204 no content`

error response status: `401 Unauthorized`, `400 bad request`, `404 not found`

#### 4. Delete list 

url: `http://todolist.local/api/list/{listId}`

method: `DELETE`

parameters:

`no request parameters`

success status: `204 no content`

error response status: `401 Unauthorized`, `404 not found`

#### 5. Get list's notes.  

url: `http://todolist.local/api/list/{listId}/notes`

method: `GET`

parameters:

`no request parameters`

success status: `200 OK`

success response: 

`[
   {
     "id": 1,
     "listId": 2,
     "status": "todo",
     "title": "second note",
     "note": "test note",
     "createdAt": {
       "date": "2020-07-06 07:37:43.000000",
       "timezone_type": 3,
       "timezone": "UTC"
     },
     "updatedAt": {
       "date": "2020-07-06 07:37:43.000000",
       "timezone_type": 3,
       "timezone": "UTC"
     }
   },
   {
     "id": 2,
     "listId": 2,
     "status": "todo",
     "title": "first note",
     "note": "test note",
     "createdAt": {
       "date": "2020-07-06 07:38:44.000000",
       "timezone_type": 3,
       "timezone": "UTC"
     },
     "updatedAt": {
       "date": "2020-07-06 07:38:44.000000",
       "timezone_type": 3,
       "timezone": "UTC"
     }
   }
 ]`

error response status: `401 Unauthorized`, `404 not found`

#### 6. Get list data.  

url: `http://todolist.local/api/list/{listId}`

method: `GET`

parameters:

`no request parameters`

success status: `200 OK`

success response: 

`{
   "id": 2,
   "userId": 1,
   "title": "test",
   "createdAt": {
     "date": "2020-07-06 07:06:08.000000",
     "timezone_type": 3,
     "timezone": "UTC"
   },
   "updatedAt": {
     "date": "2020-07-06 07:06:08.000000",
     "timezone_type": 3,
     "timezone": "UTC"
   }
 }`

error response status: `401 Unauthorized`, `404 not found`

#### 7. Get user lists.  

url: `http://todolist.local/api/lists`

method: `GET`

parameters:

`no request parameters`

success status: `200 OK`

success response: 

`[
   {
     "id": 2,
     "userId": 1,
     "title": "test",
     "createdAt": {
       "date": "2020-07-06 07:06:08.000000",
       "timezone_type": 3,
       "timezone": "UTC"
     },
     "updatedAt": {
       "date": "2020-07-06 07:06:08.000000",
       "timezone_type": 3,
       "timezone": "UTC"
     }
   },
   {
     "id": 3,
     "userId": 1,
     "title": "test 2",
     "createdAt": {
       "date": "2020-07-06 07:43:47.000000",
       "timezone_type": 3,
       "timezone": "UTC"
     },
     "updatedAt": {
       "date": "2020-07-06 07:43:47.000000",
       "timezone_type": 3,
       "timezone": "UTC"
     }
   }
 ]`

error response status: `401 Unauthorized`

#### 8. Create note 

url: `http://todolist.local/api/list/{listId}/create-note`

method: `POST`

parameters:

`title: note name`

`status: todo` : Acceptable statuses: `todo`, `todo`, `in_progress`, `done`

`note: some text`  

success status: `201 Created`

success response: 

`{
   "id": 3,
   "listId": 2,
   "status": "todo",
   "title": "first note",
   "note": null,
   "createdAt": {
     "date": "2020-07-06 07:48:29.622264",
     "timezone_type": 3,
     "timezone": "UTC"
   },
   "updatedAt": {
     "date": "2020-07-06 07:48:29.622271",
     "timezone_type": 3,
     "timezone": "UTC"
   }
 }`

error response status: `401 Unauthorized`, `400 bad request`, `404 not found`, `409 conflict`



#### 9. Update note 

url: `http://todolist.local/api/note/{noteId}`

method: `PATCH`

parameters (**At least one of them must be transferred**):

`title: note name`

`status: todo` : Acceptable statuses: `todo`, `todo`, `in_progress`, `done`

`note: some text` 


success status: `204 no content`

error response status: `401 Unauthorized`, `400 bad request`, `404 not found`

#### 10. Get note data.  

url: `http://todolist.local/api/note/{noteId}`

method: `GET`

parameters:

`no request parameters`

success status: `200 OK`

success response: 

`{
   "id": 2,
   "listId": 2,
   "status": "todo",
   "title": "first note",
   "note": "test note",
   "createdAt": {
     "date": "2020-07-06 07:38:44.000000",
     "timezone_type": 3,
     "timezone": "UTC"
   },
   "updatedAt": {
     "date": "2020-07-06 07:38:44.000000",
     "timezone_type": 3,
     "timezone": "UTC"
   }
 }`

error response status: `401 Unauthorized`, `404 not found`

#### 11. Delete note 

url: `http://todolist.local/api/note/{noteId}`

method: `DELETE`

parameters:

`no request parameters`

success status: `204 no content`

error response status: `401 Unauthorized`, `404 not found`
